function getOffset(currentPage = 1, listPerPage) {
    return (currentPage - 1) * [listPerPage];
  }
  
  function emptyOrRows(rows) {
    if (!rows) {
      return [];
    }
    return rows;
  }

  const generatePassword = () => {
    const randomNumber = Math.floor(100000000 + Math.random() * 20000000000); // Genera un número aleatorio entre 100000000 y 20000000000
    const newPassword = 'CB123'; // Obtiene los primeros 7 dígitos del número aleatorio
    const newHashedPassword = require('crypto').createHash('sha1').update(newPassword).digest('hex'); // Aplica el hash SHA1 al password generado
  
    return newHashedPassword;
  };
  
  module.exports = {
    getOffset,
    emptyOrRows,
    generatePassword
  }
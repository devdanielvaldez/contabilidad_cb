const router = require('express').Router();
const { 
    registrarAdmision,
    registrarPadre,
    consultarPadres,
    consutarGrados
} = require('../controllers/admissions');

router.post('/register/student', registrarAdmision);
router.post('/register/parent', registrarPadre);
router.get('/parents', consultarPadres);
router.get('/class', consutarGrados)

module.exports = router;
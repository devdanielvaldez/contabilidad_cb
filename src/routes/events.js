const router = require('express').Router();
const { registrarEvento,
    registrarPagoEvento,
    mostrarGananciasPorEventos,
    listarEventos,
    cambiarEstadoEvento } = require('../controllers/events');

router.post('/', registrarEvento);
router.post('/pago', registrarPagoEvento);
router.get('/ganancia', mostrarGananciasPorEventos);
router.get('/', listarEventos);
router.put('/', cambiarEstadoEvento);

module.exports = router;
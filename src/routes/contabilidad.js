const router = require('express').Router();
const { 
    registrarInventario,
    registrarVenta,
    obtenerGananciasPorMes,
    verInventario,
    cambiarEstadoPorCodigo,
    editarArticuloInventario,
    obtenerGananciasPorDia,
    register_receipts,
    getReceipts,
    getReportsReceipts,
    getStudentPendintPayment,
    getStudentPendingPayment
} = require('../controllers/contabilidad');

router.post('/register/inventario', registrarInventario);
router.post('/register/venta', registrarVenta);
router.get('/resumen/ventas', obtenerGananciasPorMes);
router.get('/inventario', verInventario);
router.get('/:codigo', cambiarEstadoPorCodigo);
router.put('/update', editarArticuloInventario)
router.post('/dia', obtenerGananciasPorDia);
router.post('/receipts', register_receipts);
router.get('/receipts/:student_id', getReceipts);
router.get('/report/receipts', getReportsReceipts);
router.get('/pendint/invoce/:student_id', getStudentPendintPayment);
router.get('/pendint/general', getStudentPendingPayment)

module.exports = router;
const router = require('express').Router();
const student = require('./students.routes');
const payments = require('./payments.routes');
const admission = require('./admissions');
const contabilidad = require('./contabilidad');
const employes = require('./employes');
const events = require('./events');
const pagosExtraordinarios = require('./pagos_extraordinarios.routes');
const utils = require('./utils.routes');
const contadora_general = require('./contadora_general');

router.use('/students', student);
router.use('/payments', payments);
router.use('/admision', admission);
router.use('/contabilidad', contabilidad);
router.use('/employes', employes);
router.use('/events', events);
router.use('/pagos/extraordinarios', pagosExtraordinarios);
router.use('/utils', utils);
router.use('/contadora-general', contadora_general);

module.exports = router;
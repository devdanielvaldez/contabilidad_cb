const router = require('express').Router();
const {
    getClass,
    getMesEscolar,
    getMaterias,
    getStudentsByClass,
    getClassById,
    getGeneralsCalificaciones,
    getStudentInfo,
    enviarMensajesATodos
} = require('../controllers/utils');

router.get('/class', getClass);
router.get('/mes', getMesEscolar);
router.get('/materias/:class_id', getMaterias);
router.get('/:id', getStudentsByClass);
router.get('/class/:id', getClassById);
router.get('/generals/calificaciones', getGeneralsCalificaciones);
router.get('/student/:id', getStudentInfo);
router.post('/send/sms', enviarMensajesATodos);

module.exports = router;
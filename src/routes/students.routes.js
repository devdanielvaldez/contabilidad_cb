const router = require('express').Router();
const { find_all_students, addMonthlyFee, registerInscription, getStudentPayments, registerPaymentAbono, registerPaymentCuota, registerMultiPaymentCuota, update_student_status, getStudentsPayments, movePaidCuotasToHistorial, consultarHistorialPago, payAllCuotas, getStudentAndParents, getStudentsClass,registrarCalificacion, getNotasStudentByClassId } = require('../controllers/students');

router.get('/find/all', find_all_students);
router.put('/add/monthly/fee/:student_id', addMonthlyFee);
router.put('/register/inscription', registerInscription);
router.get('/get/payments/:student_id',getStudentPayments);
router.post('/get/multi_students', getStudentsPayments)
router.post('/register/abono', registerPaymentAbono);
router.post('/register/cuota', registerPaymentCuota);
router.post('/register/multi/payment', registerMultiPaymentCuota);
router.put('/update/status', update_student_status);
router.get('/reset/student/:studentId', movePaidCuotasToHistorial);
router.post('/consultar/historial', consultarHistorialPago);
router.post('/pagar/cuotas', payAllCuotas);
router.get('/all/data/:student_id', getStudentAndParents);
router.get('/class', getStudentsClass);
router.post('/registrar/calificacion', registrarCalificacion);
router.post('/calificacion', getNotasStudentByClassId);

module.exports = router;
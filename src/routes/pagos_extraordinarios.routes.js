const router = require('express').Router();
const {
    registrarEvento,
    buscarEventos,
    asignarEventos,
    obtenerEstudiantesPorEvento,
    registrarPagoEvento,
    obtenerPagosPorEvento
} = require('../controllers/pagos_extraordinarios');

router.post('/registrar-evento', registrarEvento);
router.get('/', buscarEventos);
router.post('/asignar/eventos', asignarEventos);
router.get('/obtener/:evento_p_id', obtenerEstudiantesPorEvento);
router.post('/pago-evento', registrarPagoEvento);
router.get('/:evento_id', obtenerPagosPorEvento)

module.exports = router;
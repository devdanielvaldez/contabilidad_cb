const router = require('express').Router();
const {
    generarReporteReceipts,
    generarReporteVentas,
    cxc,
    getTotalMatriculas
} = require('../controllers/contadora_general');

router.get('/receipts', generarReporteReceipts);
router.get('/ventas', generarReporteVentas);
router.get('/cxc', cxc);
router.get('/total/matriculas', getTotalMatriculas);

module.exports = router;
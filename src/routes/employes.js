const router = require('express').Router();
const { getAllEmployes, saveAFP, saveSalary, saveSeguro, saveTSS, saveSocioId, saveAccount } = require('../controllers/employes');

router.get('/all', getAllEmployes);
router.post('/afp', saveAFP);
router.post('/salary', saveSalary);
router.post('/seguro', saveSeguro);
router.post('/tss', saveTSS);
router.post('/socio', saveSocioId);
router.post('/account', saveAccount)

module.exports = router;
const mysql = require('mysql2/promise');
const config = require('../../config');

async function query(sql, params) {
  const connection = await mysql.createConnection(config.db);

  try {
    const [results, ] = await connection.execute(sql, params);
    connection.end();
    connection.destroy();
    return results;

    
  } catch (err) {
    connection.end();
    connection.destroy();
    throw err;
  } finally {
    connection.end();
    connection.destroy();
  }
}

module.exports = {
  query
}
const db = require('../services/db');

const registrarEvento = async (req, res) => {
    try {
      const { descripcion, fecha, fecha_limite, monto } = req.body;
  
      // Verificar si el evento ya existe en la base de datos por su descripción y fecha
      const queryVerificarEvento = `SELECT * FROM eventos WHERE descripcion = '${descripcion}' AND fecha = '${fecha}'`;
      const eventosExistente = await db.query(queryVerificarEvento);
  
      if (eventosExistente.length > 0) {
        return res.status(400).json({
          ok: false,
          msg: 'Ya existe un evento con la misma descripción y fecha.',
        });
      }
  
      // Si el evento no existe, proceder a registrarlo
      const queryRegistrarEvento = `INSERT INTO eventos (descripcion, fecha, fecha_limite, monto, estado) 
                                    VALUES ('${descripcion}', '${fecha}', '${fecha_limite}', ${monto}, 1)`;
      await db.query(queryRegistrarEvento);
  
      return res.status(201).json({
        ok: true,
        msg: 'Evento registrado exitosamente.',
      });
    } catch (err) {
      console.log('Error in registrarEvento:', err);
      return res.status(500).json({
        ok: false,
        msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
        error: err,
      });
    }
  };

  const registrarPagoEvento = async (req, res) => {
    try {
      const { evento_id, fecha_pago } = req.body;
  
      // Obtener el evento por su ID
      const queryEvento = `SELECT * FROM eventos WHERE evento_id = ${evento_id}`;
      const eventos = await db.query(queryEvento);
  
      if (eventos.length === 0) {
        return res.status(404).json({
          ok: false,
          msg: 'El evento especificado no existe.',
        });
      }
  
      const evento = eventos[0];
      const fechaLimitePago = new Date(evento.fecha_limite);
      const fechaPago = new Date(fecha_pago);
  
      if (fechaPago > fechaLimitePago) {
        return res.status(400).json({
          ok: false,
          msg: 'La fecha de pago debe ser antes de la fecha límite de pago del evento.',
        });
      }
  
      // Si la fecha de pago es válida, registrar el pago del evento
      const queryRegistrarPago = `INSERT INTO pago_eventos (evento_id, fecha_pago) VALUES (${evento_id}, '${fecha_pago}')`;
      await db.query(queryRegistrarPago);
  
      return res.status(201).json({
        ok: true,
        msg: 'Pago registrado exitosamente.',
      });
    } catch (err) {
      console.log('Error in registrarPagoEvento:', err);
      return res.status(500).json({
        ok: false,
        msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
        error: err,
      });
    }
  };

  const mostrarGananciasPorEventos = async (req, res) => {
    try {
      // Obtener todos los pagos de eventos
      const queryPagos = `SELECT * FROM pago_eventos`;
      const pagos = await db.query(queryPagos);
  
      // Calcular las ganancias sumando los montos de los eventos pagados
      let ganancias = 0;
      pagos.forEach((pago) => {
        ganancias += pago.monto;
      });
  
      return res.status(200).json({
        ok: true,
        ganancias: ganancias,
      });
    } catch (err) {
      console.log('Error in mostrarGananciasPorEventos:', err);
      return res.status(500).json({
        ok: false,
        msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
        error: err,
      });
    }
  };

  const listarEventos = async (req, res) => {
    try {
      // Obtener todos los eventos de la base de datos
      const queryEventos = `SELECT * FROM eventos WHERE estado = 1`;
      const eventos = await db.query(queryEventos);
  
      return res.status(200).json({
        ok: true,
        eventos: eventos,
      });
    } catch (err) {
      console.log('Error in listarEventos:', err);
      return res.status(500).json({
        ok: false,
        msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
        error: err,
      });
    }
  };

  const cambiarEstadoEvento = async (req, res) => {
    try {
        const { evento_id } = req.body;
      const query = `UPDATE eventos SET estado = 0 WHERE evento_id = ${evento_id}`;
  
      // Ejecutar la consulta en la base de datos
      await db.query(query);
  
      // Si llegamos aquí, el cambio de estado fue exitoso
      console.log(`Se cambió el estado del evento con evento_id ${evento_id} a 0.`);
      return res.status(200).json({
        ok: true,
        msg: "Se ha eliminado el evento"
      });
    } catch (err) {
      console.log('Error en cambiarEstadoEvento:', err);
      throw err; // Puedes manejar el error según tus necesidades
    }
  };
  

  
  module.exports = {
    registrarEvento,
    registrarPagoEvento,
    mostrarGananciasPorEventos,
    listarEventos,
    cambiarEstadoEvento
  }
  
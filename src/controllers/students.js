const helper = require('../../helper');
const db = require('../services/db'); 

const find_all_students = async (req, res) => {
  try {
    const { id, name, last_name, email, username } = req.query;

    let query = `SELECT student_id, first_name, last_name, phone, email, since, username, parent_id, cuotas_mensuales, inscripcion_monto, inscripcion_estado, status, monto_abonado_inscripcion
                   FROM student
                   WHERE 1 = 1`;

    if (id) {
      query += ` AND student_id = ${id}`;
    }

    if (name) {
      query += ` AND first_name LIKE '%${name}%'`;
    }

    if (last_name) {
      query += ` AND last_name LIKE '%${last_name}%'`;
    }

    if (email) {
      query += ` AND email LIKE '%${email}%'`;
    }

    if (username) {
      query += ` AND username LIKE '%${username}%'`;
    }

    const rows = await db.query(query);
    const data = helper.emptyOrRows(rows);

    return res.status(200).json({
      ok: true,
      data: data
    });
  } catch (err) {
    console.log('Error in find_all_students:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const addMonthlyFee = async (req, res) => {
  try {
    const { student_id } = req.params;
    const { cuota } = req.body;

    if (cuota < 1800) return res.status(400).json({
      ok: false,
      msg: "Debe ingresar un monto mayor o igual 1,800 DOP."
    })

    // Validar existencia del estudiante
    const student = await db.query(
      `SELECT * FROM student WHERE student_id = ${student_id}`
    );
    if (student.length === 0) {
      return res.status(404).json({
        ok: false,
        msg: 'Estudiante no encontrado',
      });
    }

    // Validar el valor de cuotas_mensuales
    const cuotasMensuales = student[0].cuotas_mensuales;
    if (cuotasMensuales !== null && cuotasMensuales > 0) {
      return res.status(400).json({
        ok: false,
        msg: 'El estudiante ya tiene cuotas mensuales registradas',
      });
    }

    // Validar la inscripción
    const inscripcionEstado = student[0].inscripcion_estado;
    const inscripcionMonto = student[0].inscripcion_monto;

    if (inscripcionEstado === 0 && inscripcionMonto == null) {
      return res.status(400).json({
        ok: false,
        msg: 'Primero se debe pagar la inscripción',
      });
    }

    if (inscripcionMonto === 0 || inscripcionMonto == null) {
      return res.status(400).json({
        ok: false,
        msg: 'Aún no hay monto asignado a la inscripción. Debes completar el proceso de inscripción primero.',
      });
    }

    // Actualizar cuotas_mensuales con el valor recibido
    await db.query(
      `UPDATE student SET cuotas_mensuales = ${cuota} WHERE student_id = ${student_id}`
    );

    // Agregar registros a la tabla cuotas_mensuales
    const meses = [
      'Septiembre',
      'Octubre',
      'Noviembre',
      'Diciembre',
      'Enero',
      'Febrero',
      'Marzo',
      'Abril',
      'Mayo',
      'Junio',
    ];

    for (const mes of meses) {
      await db.query(
        `INSERT INTO cuotas_mensuales (student_id, mes, estado, monto_a_pagar, monto_pagado) VALUES (${student_id}, '${mes}', 'PE', ${cuota}, 0)`
      );
    }

    return res.status(200).json({
      ok: true,
      msg: 'Cuota mensual agregada exitosamente',
    });
  } catch (err) {
    console.log('Error in addMonthlyFee:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const registerInscription = async (req, res) => {
  try {
    const { student_id, monto, pagado } = req.body;

    // Validar existencia del estudiante
    const student = await db.query(
      `SELECT * FROM student WHERE student_id = ${student_id}`
    );

    if (student.length === 0) {
      return res.status(404).json({
        ok: false,
        msg: 'Estudiante no encontrado',
      });
    }

    // Validar si el estudiante ya ha pagado la inscripción
    const inscripcionPagado = student[0].inscripcion_estado;
    if (inscripcionPagado) {
      return res.status(400).json({
        ok: false,
        msg: 'El estudiante ya ha pagado la inscripción',
      });
    }

    // Actualizar la inscripción en la tabla student
    let updateQuery = `UPDATE student SET inscripcion_estado = ${pagado ? 1 : 0}`;

    if (monto !== null) {
      const inscripcionMonto = student[0].inscripcion_monto;
      if (inscripcionMonto > 0) {
        return res.status(400).json({
          ok: false,
          msg: 'El estudiante ya tiene un monto de inscripción registrado',
        });
      }

      updateQuery += `, inscripcion_monto = ${monto}`;
    }

    updateQuery += ` WHERE student_id = ${student_id}`;

    await db.query(updateQuery);

    // Registrar el pago en el historial_de_pago
    const fechaPago = new Date(); // Obtén la fecha actual
    const tipoPago = "Inscripción"; // Define el tipo de pago

    const insertQuery = `INSERT INTO historial_de_pago (student_id, fecha_pago, monto, tipo_pago) VALUES (${student_id}, '${fechaPago}', ${monto}, '${tipoPago}')`;
    await db.query(insertQuery);

    const enrollQuery = `INSERT INTO enroll (
      enroll_code,
      student_id,
      class_id,
      section_id,
      roll,
      date_added,
      year
    ) VALUES (?, ?, ?, ?, ?, ?, ?)`;

    const enrollValues = [generarCodigoAlfanumerico(7), student[0].student_id, student[0].class_id, 1, 'N/A', "1689866981", "2023"];
    await db.query(enrollQuery, enrollValues);

    return res.status(200).json({
      ok: true,
      msg: 'Inscripción registrada exitosamente',
      student: student[0],
      monto: monto == null ? student[0].inscripcion_monto : monto,
      pagado: pagado
    });
  } catch (err) {
    console.log('Error in registerInscription:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

function generarCodigoAlfanumerico(y) {
  const caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let codigo = '';

  for (let i = 0; i < y; i++) {
    const randomIndex = Math.floor(Math.random() * caracteres.length);
    codigo += caracteres.charAt(randomIndex);
  }

  return codigo;
}

const getStudentPayments = async (req, res) => {
  try {
    const { student_id } = req.params;

    // Consultar información del estudiante
    const student = await db.query(
      `SELECT * FROM student WHERE student_id = ${student_id}`
    );

    if (student.length === 0) {
      return res.status(404).json({
        ok: false,
        msg: 'Estudiante no encontrado',
      });
    }

    // Obtener cuotas mensuales del estudiante
    const cuotasMensuales = await db.query(
      `SELECT * FROM cuotas_mensuales WHERE student_id = ${student_id}`
    );

    // Calcular monto pagado y monto pendiente de pago
    let montoPagado = 0;
    cuotasMensuales.forEach((cuota) => {
      const montoPagadoCuota = parseFloat(cuota.monto_pagado) || 0;
      montoPagado += montoPagadoCuota;
      const montoAPagarCuota = parseFloat(cuota.monto_a_pagar) || 0;
      cuota.pendiente_de_pago = montoAPagarCuota - montoPagadoCuota;
    });

    const montoPendiente = cuotasMensuales.reduce((total, cuota) => {
      const montoPagadoCuota = parseFloat(cuota.monto_pagado) || 0;
      const montoAPagarCuota = parseFloat(cuota.monto_a_pagar) || 0;
      if (montoPagadoCuota !== montoAPagarCuota) {
        return total + (montoAPagarCuota - montoPagadoCuota);
      }
      return total;
    }, 0);


    // Verificar y corregir el campo "mes" si está en blanco
    cuotasMensuales.forEach((cuota) => {
      if (cuota.mes === '') {
        cuota.mes = 'diciembre';
      }
    });

    // Obtener información de la inscripción
    const inscripcionMonto = student[0].inscripcion_monto;
    const inscripcionPagado = student[0].inscripcion_estado;

    // Devolver respuesta con los datos actualizados
    return res.status(200).json({
      ok: true,
      student_id: student[0].student_id,
      general: {
        first_name: student[0].first_name,
        last_name: student[0].last_name
      },
      cuotas: cuotasMensuales,
      monto_pagado: montoPagado,
      monto_pendiente: montoPendiente,
      monto_abonado_inscripcion: student[0].monto_abonado_inscripcion,
      inscripcion: {
        monto: inscripcionMonto,
        estado: inscripcionPagado,
      },
    });
  } catch (err) {
    console.log('Error in getStudentPayments:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const getStudentsPayments = async (req, res) => {
  try {
    const { student_ids } = req.body;

    // Consultar información de los estudiantes
    const students = await db.query(
      `SELECT * FROM student WHERE student_id IN (${student_ids.join(',')})`
    );

    if (students.length === 0) {
      return res.status(404).json({
        ok: false,
        msg: 'No se encontraron estudiantes',
      });
    }

    const response = [];
    const studentsWithCuotas = [];

    for (const student of students) {
      const student_id = student.student_id;

      // Obtener cuotas mensuales del estudiante
      const cuotasMensuales = await db.query(
        `SELECT * FROM cuotas_mensuales WHERE student_id = ${student_id}`
      );

      // Filtrar las cuotas donde monto_pendiente y monto_pagado no sean iguales
      const cuotasPendientes = cuotasMensuales.filter(
        cuota => parseFloat(cuota.monto_pagado) !== parseFloat(cuota.monto_a_pagar)
      );

      if (cuotasPendientes.length > 0) {
        studentsWithCuotas.push(student);
        // Verificar y corregir el campo "mes" si está en blanco
        cuotasPendientes.forEach(cuota => {
          if (cuota.mes === '') {
            cuota.mes = 'diciembre';
          }
        });
      }
    }

    if (studentsWithCuotas.length === 1 || studentsWithCuotas.length === 0) {
      return res.status(400).json({
        ok: false,
        msg: `No es posible realizar un pago múltiple, ya que uno de los estudiantes no posee cuotas disponibles para pagar.`,
      });
    }

    for (const student of studentsWithCuotas) {
      const student_id = student.student_id;

      // Obtener cuotas mensuales del estudiante
      const cuotasMensuales = await db.query(
        `SELECT * FROM cuotas_mensuales WHERE student_id = ${student_id}`
      );

      // Filtrar las cuotas donde monto_pendiente y monto_pagado no sean iguales
      const cuotasPendientes = cuotasMensuales.filter(
        cuota => parseFloat(cuota.monto_pagado) !== parseFloat(cuota.monto_a_pagar)
      );

      // Verificar y corregir el campo "mes" si está en blanco
      cuotasPendientes.forEach(cuota => {
        if (cuota.mes === '') {
          cuota.mes = 'diciembre';
        }
      });

      response.push({
        student_id: student.student_id,
        general: {
          first_name: student.first_name,
          last_name: student.last_name
        },
        cuotas: cuotasPendientes
      });
    }

    return res.status(200).json({
      ok: true,
      data: response
    });
  } catch (err) {
    console.log('Error in getStudentPayments:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const registerPaymentAbono = async (req, res) => {
  try {
    const { student_id, monto, concepto } = req.body;

    // Verificar si el estudiante existe
    const student = await db.query(
      `SELECT * FROM student WHERE student_id = ${student_id}`
    );

    if (student.length === 0) {
      return res.status(404).json({
        ok: false,
        msg: 'Estudiante no encontrado',
      });
    }

    // Verificar si el concepto es válido
    const validConcepts = ['Abono a inscripcion', 'Abono a colegiatura', 'Graduacion', 'Excursiones', 'Examenes'];
    if (!validConcepts.includes(concepto)) {
      return res.status(400).json({
        ok: false,
        msg: 'Concepto inválido',
      });
    }

    // Obtener la fecha actual
    const fecha = new Date();

    // Insertar el abono en la tabla de abonos
    await db.query(
      `INSERT INTO abonos (student_id, monto, concepto, fecha)
      VALUES (${student_id}, ${monto}, '${concepto}', '${fecha.toISOString()}')`
    );

    // Actualizar campos en la tabla student si el concepto es "Abono a inscripcion"
    if (concepto === 'Abono a inscripcion') {
      const inscripcionMonto = student[0].inscripcion_monto || 0;
      const totalInscripcionMonto = inscripcionMonto + monto;
      const inscripcionEstado = req.body.inscripcion_estado ? 1 : 0;

      await db.query(
        `UPDATE student
        SET inscripcion_estado = ${inscripcionEstado},
            monto_abonado_inscripcion = monto_abonado_inscripcion + ${monto}
        WHERE student_id = ${student_id}`
      );
    }

    // Actualizar pagos de colegiatura en la tabla cuotas_mensuales si el concepto es "Abono a colegiatura"
    if (concepto === 'Abono a colegiatura') {
      const cuotasMensuales = await db.query(
        `SELECT * FROM cuotas_mensuales WHERE student_id = ${student_id}`
      );

      let montoRestante = monto;

      for (let cuota of cuotasMensuales) {
        if (montoRestante <= 0) {
          break;
        }
    
        const montoPendiente = cuota.monto_a_pagar - (cuota.monto_pagado || 0);
    
        if (montoPendiente > 0) {
          const montoAbonado = Math.min(montoRestante, montoPendiente);
          cuota.monto_pagado = (cuota.monto_pagado || 0) + montoAbonado;
          montoRestante -= montoAbonado;
        }
    
        // Registrar el pago en el historial_de_pago
        const fechaPago = new Date(); // Obtén la fecha actual
        const tipoPago = "Abono"; // Define el tipo de pago
    
        const insertQuery = `INSERT INTO historial_de_pago (student_id, fecha_pago, monto, tipo_pago) VALUES (${student_id}, '${fechaPago}', ${monto}, '${tipoPago}')`;
        await db.query(insertQuery);

              // Actualizar el estado de la cuota si aún queda monto por abonar
      if (montoRestante > 0) {
        cuota.estado = 'PA'; // Cambiar el estado de la cuota a "PA" (pagada)
      }
      
      await db.query(
        `UPDATE cuotas_mensuales
         SET monto_pagado = ${cuota.monto_pagado}, estado = '${cuota.estado}'
         WHERE id = ${cuota.id}`
      );
      }
    
    }

    return res.status(200).json({
      ok: true,
      msg: 'Abono registrado exitosamente',
      student: student[0],
      monto: monto,
      concepto: concepto
    });
  } catch (err) {
    console.log('Error in registerPayment:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const registerPaymentCuota = async (req, res) => {
  try {
    const { cuota_id, monto } = req.body;

    // Verificar si la cuota existe
    const cuota = await db.query(
      `SELECT * FROM cuotas_mensuales WHERE id = ${cuota_id}`
    );

    if (cuota.length === 0) {
      return res.status(404).json({
        ok: false,
        msg: 'Cuota no encontrada',
      });
    }

    // Calcular el monto pendiente de pago
    const montoPendiente = cuota[0].monto_a_pagar - cuota[0].monto_pagado;

    // Verificar si el monto ingresado es mayor al monto pendiente de pago
    if (monto > montoPendiente) {
      const extra = monto - montoPendiente;
      return res.status(400).json({
        ok: false,
        msg: `El monto ingresado es mayor al monto pendiente de pago en la cuota. Debe realizar un abono de $${extra} para poder continuar.`,
      });
    }

    // Verificar si el monto ingresado es menor al monto pendiente de pago
    if (monto < montoPendiente) {
      return res.status(400).json({
        ok: false,
        msg: 'El monto ingresado es menor al monto pendiente de pago en la cuota.',
      });
    }

    // Actualizar el monto pagado de la cuota actual
    await db.query(
      `UPDATE cuotas_mensuales
      SET monto_pagado = monto_pagado + ${monto}, estado = 'PA'
      WHERE id = ${cuota_id}`
    );

    // Registrar el pago en el historial_de_pago
    const fechaPago = new Date(); // Obtén la fecha actual
    const tipoPago = "Pago Cuota"; // Define el tipo de pago

    const insertQuery = `INSERT INTO historial_de_pago (student_id, fecha_pago, monto, tipo_pago) VALUES (${cuota[0].student_id}, '${fechaPago}', ${monto}, '${tipoPago}')`;
    await db.query(insertQuery);

    const student = await db.query(`SELECT * FROM student WHERE student_id = ${cuota[0].student_id}`);
    console.log('finish')
    return res.status(200).json({
      ok: true,
      msg: 'Pago registrado exitosamente',
      student: student[0],
      cuota: cuota[0]
    });
  } catch (err) {
    console.log('Error in registerPaymentCuota:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const registerMultiPaymentCuota = async (req, res) => {
  try {
    const { pagos } = req.body;

    const pagosRegistrados = [];

    for (const pago of pagos) {
      const { student_id, cuota_id } = pago;

      // Verificar si la cuota existe
      const cuota = await db.query(
        `SELECT * FROM cuotas_mensuales WHERE id = ${cuota_id}`
      );

      if (cuota.length === 0) {
        pagosRegistrados.push({
          student_id,
          cuota_id,
          success: false,
          message: 'Cuota no encontrada',
        });
        continue;
      }

      // Calcular el monto pendiente de pago
      const montoPendiente = cuota[0].monto_a_pagar - cuota[0].monto_pagado;

      // Actualizar el monto pagado de la cuota actual
      await db.query(
        `UPDATE cuotas_mensuales
        SET monto_pagado = monto_pagado + ${montoPendiente}, estado = 'PA'
        WHERE id = ${cuota_id}`
      );

      // Registrar el pago en el historial_de_pago
      const fechaPago = new Date(); // Obtén la fecha actual
      const tipoPago = "Pago Cuota"; // Define el tipo de pago

      const insertQuery = `INSERT INTO historial_de_pago (student_id, fecha_pago, monto, tipo_pago) VALUES (${student_id}, '${fechaPago}', ${cuota[0].monto_a_pagar}, '${tipoPago}')`;
      await db.query(insertQuery);

      const student = await db.query(`SELECT * FROM student WHERE student_id = ${student_id}`);

      pagosRegistrados.push({
        student_id,
        cuota_id,
        success: true,
        message: 'Pago registrado exitosamente',
        student: student[0],
        cuota: cuota[0],
      });
    }

    console.log('finish');
    return res.status(200).json({
      ok: true,
      pagos: pagosRegistrados,
    });
  } catch (err) {
    console.log('Error in registerPaymentCuota:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const movePaidCuotasToHistorial = async (req, res) => {
  try {
    const { studentId } = req.params;

    // Verificar si todas las cuotas del estudiante están pagadas
    const cuotasPendientes = await db.query(
      `SELECT * FROM cuotas_mensuales WHERE student_id = ${studentId} AND estado <> 'PA'`
    );

    if (cuotasPendientes.length > 0) {
      // No todas las cuotas están pagadas, no se realiza ninguna acción
      return res.status(400).json({
        ok: false,
        msg: 'No se pueden mover las cuotas al historial. Aún existen cuotas pendientes de pago.',
      });
    }

    // Obtener el año actual
    const currentYear = new Date().getFullYear();

    // Mover las cuotas pagadas a la tabla historial_cuotas y asignar el semestre como el año actual
    await db.query(
      `INSERT INTO historial_cuotas (student_id, semestre, mes, monto_a_pagar, monto_pagado)
       SELECT student_id, ${currentYear}, mes, monto_a_pagar, monto_pagado
       FROM cuotas_mensuales
       WHERE student_id = ${studentId}`
    );

    // Eliminar los registros de cuotas_mensuales correspondientes al estudiante
    await db.query(
      `DELETE FROM cuotas_mensuales
       WHERE student_id = ${studentId}`
    );

    // Actualizar los valores del estudiante a cero
    await db.query(
      `UPDATE student
       SET inscripcion_monto = null,
           inscripcion_estado = 0,
           cuotas_mensuales = null
       WHERE student_id = ${studentId}`
    );

    return res.status(200).json({
      ok: true,
      msg: 'Estudiante disponible para re-inscripción',
    });
  } catch (err) {
    console.log('Error in movePaidCuotasToHistorial:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const update_student_status = async (req, res) => {
  try {
    const { student_id, status } = req.body;

    if(status == "i") {
      const query_in = `DELETE FROM enroll
      WHERE student_id = ${student_id}`;;

      await db.query(query_in);
      const query = `UPDATE student SET status = '${status}' WHERE student_id = ${student_id}`;

      await db.query(query);

      return res.status(200).json({
        ok: true,
        msg: 'El estado del estudiante ha sido actualizado correctamente, ya se encuentra en estado INACTIVO',
      })
    }

    const query = `UPDATE student SET status = '${status}' WHERE student_id = ${student_id}`;

    await db.query(query);

    return res.status(200).json({
      ok: true,
      msg: 'El estado del estudiante ha sido actualizado correctamente.',
    });
  } catch (err) {
    console.log('Error in update_student_status:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const consultarHistorialPago = async (req, res) => {
  try {
    const { student_id } = req.body;

    // Consultar el historial de pago del estudiante
    const query = `SELECT fecha_pago, monto, tipo_pago FROM historial_de_pago WHERE student_id = ${student_id}`;
    const historialPago = await db.query(query);

    if (historialPago.length === 0) {
      return res.status(404).json({
        ok: false,
        msg: 'No se encontró historial de pago para el estudiante',
      });
    }

    return res.status(200).json({
      ok: true,
      historialPago,
    });
  } catch (err) {
    console.log('Error in consultarHistorialPago:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const payAllCuotas = async (req, res) => {
  try {
    const { student_id } = req.body;

    // Verificar si el estudiante existe
    const student = await db.query(
      `SELECT * FROM student WHERE student_id = ${student_id}`
    );

    if (student.length === 0) {
      return res.status(404).json({
        ok: false,
        msg: 'Estudiante no encontrado',
      });
    }

    // Obtener todas las cuotas pendientes del estudiante
    const cuotasPendientes = await db.query(
      `SELECT * FROM cuotas_mensuales WHERE student_id = ${student_id} AND estado = 'PE'`
    );

    if (cuotasPendientes.length === 0) {
      return res.status(400).json({
        ok: false,
        msg: 'El estudiante no tiene cuotas pendientes por pagar',
      });
    }

    let totalPagado = 0;
    const fechaPago = new Date();
    const tipoPago = "Abono";

    // Marcar todas las cuotas como pagadas y registrar en el historial de pago
    for (let cuota of cuotasPendientes) {
      const montoPendiente = cuota.monto_a_pagar - (cuota.monto_pagado || 0);
      totalPagado += montoPendiente;

      // Actualizar la cuota como pagada en la base de datos
      await db.query(
        `UPDATE cuotas_mensuales
         SET monto_pagado = ${cuota.monto_a_pagar}, estado = 'PA'
         WHERE id = ${cuota.id}`
      );

      // Registrar el pago en el historial_de_pago
      const insertQuery = `INSERT INTO historial_de_pago (student_id, fecha_pago, monto, tipo_pago) VALUES (${student_id}, '${fechaPago.toISOString()}', ${montoPendiente}, '${tipoPago}')`;
      await db.query(insertQuery);
    }

    return res.status(200).json({
      ok: true,
      msg: 'Todas las cuotas han sido pagadas exitosamente',
      student: student[0],
      totalPagado: totalPagado,
      concepto: 'Pago de colegiatura completa',
      pagado: true
    });
  } catch (err) {
    console.log('Error in payAllCuotas:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const getStudentAndParents = async (req, res) => {
  try {
    const { student_id } = req.params;

    const existStudent = await db.query(`SELECT * FROM student WHERE student_id = ${student_id}`);

    if(existStudent.length == 0) return res.status(404).json({
      ok: false,
      msg: "El estudiante que desea consultar no existe en el sistema"
    });

    const parentOne = await db.query(`SELECT * FROM parent WHERE parent_id = ${existStudent[0].parent_id}`);
    const parentTwo = await db.query(`SELECT * FROM parent WHERE parent_id = ${existStudent[0].mother_id}`);

    return res.status(200).json({
      ok: true,
      data: {
        student: existStudent[0],
        father: parentOne[0] || null,
        mother: parentTwo[0] || null
      }
    });
  } catch(err) {
    console.log('Error in getStudentAndParents:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
}

const getStudentsClass = async(req, res) => {
  try {
        // Consulta SQL para obtener clases con estudiantes (mismo código que se mostró anteriormente)
        const consultaSQL = `
        SELECT c.class_id, c.name AS class_name, GROUP_CONCAT(e.student_id) AS student_ids
        FROM class c
        LEFT JOIN enroll e ON c.class_id = e.class_id
        GROUP BY c.class_id, c.name
      `;
  
      // Ejecuta la consulta en la base de datos
      const resultados = await db.query(consultaSQL);
  
      // Procesa los resultados para construir el objeto deseado
      const clasesConEstudiantes = resultados.map((fila) => {
        const classId = fila.class_id;
        const className = fila.class_name;
        const studentIds = fila.student_ids ? fila.student_ids.split(',').map(Number) : [];
  
        return {
          classId,
          className,
          students: studentIds,
        };
      });
  
      res.status(200).json(clasesConEstudiantes);
  } catch(err) {
    console.log('Error in getStudentsClass:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
}

const registrarCalificacion = async(req, res) => {
  try {
    const { student_id, mes_id, nota, materia_id } = req.body;

    // Consulta SQL para verificar si ya existe un registro con los mismos valores
    const consultaVerificar = `
      SELECT * FROM notas_escolares
      WHERE student_id = ? AND mes_id = ? AND materia_id = ?
    `;
    
    const exist = await db.query(consultaVerificar, [student_id, mes_id, materia_id]);

    if(exist.length > 0) {
        // Si ya existe un registro, actualizarlo
        const idExistente = exist[0].nota_id; // Suponiendo que tienes un campo "id" en tu tabla
        const consultaActualizar = `
          UPDATE notas_escolares
          SET nota = ?
          WHERE nota_id = ?
        `;
    
        await db.query(consultaActualizar, [nota, idExistente])
    
          console.log('Calificación actualizada con éxito');
          return res.status(200).json({ message: 'Calificación actualizada con éxito' });
      } else {
        // Si no existe un registro, insertarlo
        const consultaInsertar = `
          INSERT INTO notas_escolares (student_id, mes_id, nota, materia_id)
          VALUES (?, ?, ?, ?)
        `;
    
        await db.query(consultaInsertar, [student_id, mes_id, nota, materia_id]);
          console.log('Calificación registrada con éxito');
          return res.status(200).json({ message: 'Calificación registrada con éxito' });
      }   
 } catch(err) {
    console.log('Error in registrarCalificacion:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
}

const getNotasStudentByClassId = async(req, res) => {
  try {
    const { student, mes, materia } = req.body;

    const query = `SELECT * FROM notas_escolares WHERE student_id = ${student} AND mes_id = ${mes} AND materia_id = ${materia}`;

    const result = await db.query(query);

    return res.status(200).json({
      ok: true,
      data: result
    });
  } catch(err) {
    console.log('Error in getNotasStudentByClassId:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
}

module.exports = {
  find_all_students,
  addMonthlyFee,
  registerInscription,
  getStudentPayments,
  registerPaymentAbono,
  registerPaymentCuota,
  registerMultiPaymentCuota,
  update_student_status,
  getStudentsPayments,
  movePaidCuotasToHistorial,
  consultarHistorialPago,
  payAllCuotas,
  getStudentAndParents,
  getStudentsClass,
  registrarCalificacion,
  getNotasStudentByClassId
}
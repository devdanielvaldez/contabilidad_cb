const helper = require('../../helper');
const db = require('../services/db');
const twilio = require('twilio');

// Configura las credenciales de Twilio
const accountSid = 'ACd709210acb89d39a145de9f80a9222ce';
const authToken = '8c6705c47b51e5ac893653f0cdc27709';
const client = new twilio(accountSid, authToken);

async function enviarMensaje(numeroDeTelefono, mensaje) {
    try {
      const result = await client.messages.create({
        body: mensaje,
        from: '+13203810687', // Debes configurar un número en Twilio y usarlo aquí
        to: '+1' + numeroDeTelefono,
      });
  
      console.log(`Mensaje enviado a ${numeroDeTelefono}: ${result.sid}`);
    } catch (error) {
      console.error(`Error al enviar mensaje a ${numeroDeTelefono}:`, error);
    }
  }

const enviarMensajesATodos = async (req, res) => {
    try {
      // Consulta la base de datos para obtener los números de teléfono de la tabla "parent"
      const numerosDeTelefono = await consultarNumerosDeTelefono(req.body.receiver);

    // await enviarMensaje(numerosDeTelefono, req.body.msg);
  
      // Itera a través de los números de teléfono y envía un mensaje a cada uno
      for (const numeroDeTelefono of numerosDeTelefono) {
        console.log(req.body.msg, numeroDeTelefono);

        const result = await client.messages.create({
            body: req.body.msg,
            from: '+13203810687', // Debes configurar un número en Twilio y usarlo aquí
            to: numeroDeTelefono.phone,
          });
      
          console.log(`Mensaje enviado a ${numeroDeTelefono}: ${result.sid}`);
          console.log('Mensajes enviados exitosamente a todos los números de teléfono.');
      }

      return res.status(200).json({
        ok: true,
        msg: 'Mensaje enviado correctamente'
      });
    } catch (error) {
      console.error('Error al enviar mensajes:', error);
      return res.status(500).json({
        ok: false,
        msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
        error: err,
    });
    }
  };

  async function consultarNumerosDeTelefono(receiver) {
      // Realiza una consulta SQL para obtener los números de teléfono desde la tabla "parent"

      const consultaSQL = `SELECT phone FROM ${receiver == 'admin' ? 'admin' : receiver == 'teacher' ? 'teacher' : receiver == 'student' ? 'student' : receiver == 'parent' ? 'parent' : receiver == 'accountant' ? 'accountant' : 'librarian'}`;

      const result = await db.query(consultaSQL);
      const numerosDeTelefono = result.map((resultado) => ({
        phone: '+1' + resultado.phone,
      }));

      return numerosDeTelefono;
  }

const getClass = async (req, res) => {
    try {
        const classGet = await db.query('SELECT * FROM class');

        return res.status(200).json({
            ok: true,
            data: classGet
        });
    } catch (err) {
        console.log('Error in getClass:', err);
        return res.status(500).json({
            ok: false,
            msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
            error: err,
        });
    }
}

const getSemestres = async (req, res) => {
    try {
        const consultaSQL = 'SELECT * FROM semestre';

        const result = await db.query(consultaSQL);

        console.log('Semestres obtenidos con éxito');
        return res.status(200).json({
            ok: true,
            data: result
        });
    } catch (err) {
        console.log('Error in getSemestres:', err);
        return res.status(500).json({
            ok: false,
            msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
            error: err,
        });
    }
}

const getAnio = async (req, res) => {
    try {
        const consultaSQL = 'SELECT * FROM ano_escolar';

        const result = await db.query(consultaSQL);

        console.log('Anios obtenidos con éxito');
        return res.status(200).json({
            ok: true,
            data: result
        });
    } catch (err) {
        console.log('Error in getAnio:', err);
        return res.status(500).json({
            ok: false,
            msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
            error: err,
        });
    }
}

const getMesEscolar = async (req, res) => {
    try {
        const consultaSQL = 'SELECT * FROM mes_escolar';

        const result = await db.query(consultaSQL);

        console.log('Mes Escolar obtenidos con éxito');
        return res.status(200).json({
            ok: true,
            data: result
        });
    } catch (err) {
        console.log('Error in getMesEscolar:', err);
        return res.status(500).json({
            ok: false,
            msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
            error: err,
        });
    }
}

const getMaterias = async (req, res) => {
    try {
        const classId = parseInt(req.params.class_id);

        let query = '';
        if (classId >= 1 && classId <= 10) {
            query = 'SELECT * FROM materias_escolares WHERE nombre_materia IN ("Lengua Española", "Ingles", "Fances", "Matemática", "Ciencias Sociales", "Ciencias de la Naturaleza", "Formación Integral Humana y Religiosa", "Educación Fisica", "Educación Artística")';
        } else if (classId >= 11 && classId <= 13) {
            query = 'SELECT * FROM materias_escolares WHERE nombre_materia IN ("Lengua Española", "Ingles", "Frances", "Matemática", "Ciencias Sociales", "Ciencias de la Naturaleza", "Formación Integral Humana y Religiosa", "Educación Fisica", "Educación Artística")';
        } else if (classId === 14) {
            query = 'SELECT * FROM materias_escolares WHERE nombre_materia IN ("Lengua Española", "Ingles", "Frances", "Matemática", "Ciencias Sociales", "Ciencias de la Naturaleza", "Formación Integral Humana y Religiosa", "Educación Fisica", "Educación Artística", "Mat. Financiera y tecnologíca")';
        } else if (classId === 15) {
            query = 'SELECT * FROM materias_escolares WHERE nombre_materia IN ("Lengua Española", "Ingles", "Frances", "Matemática", "Ciencias Sociales", "Ciencias de la Naturaleza", "Formación Integral Humana y Religiosa", "Educación Fisica", "Educación Artística", "Estadistica, Probabilidad y tecnología")';
        } else if (classId === 16) {
            query = 'SELECT * FROM materias_escolares WHERE nombre_materia IN ("Lengua Española", "Ingles", "Frances", "Matemática", "Ciencias Sociales", "Ciencias de la Naturaleza", "Formación Integral Humana y Religiosa", "Educación Fisica", "Educación Artística", "Trigonometria, Cálculo Diferencial y Tecnología")';
        } else {
            return res.status(400).json({
                ok: false,
                msg: 'El class_id no es válido.'
            });
        }

        const result = await db.query(query);

        console.log('Materias obtenidas con éxito');
        return res.status(200).json({
            ok: true,
            data: result
        });
    } catch (err) {
        console.log('Error in getMaterias:', err);
        return res.status(500).json({
            ok: false,
            msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
            error: err,
        });
    }
}

const getStudentsByClass = async (req, res) => {
    try {
        console.log(req.params);
        if (req.params.id == "" || req.params.id == null || req.params.id == undefined || req.params.id == 'null') {
            const query = `SELECT * FROM student`;

            const results = await db.query(query);

            return res.status(200).json({
                ok: true,
                data: results
            });
        }

        const query = `SELECT * FROM student WHERE student_id IN (
            SELECT student_id FROM enroll WHERE class_id = ${req.params.id}
          );`

        const results = await db.query(query);

        return res.status(200).json({
            ok: true,
            data: results
        });
    } catch (err) {
        console.log('Error in getStudentsByClass:', err);
        return res.status(500).json({
            ok: false,
            msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
            error: err,
        });
    }
}

const getClassById = async (req, res) => {
    try {
        const query = `SELECT * FROM materias_escolares WHERE materia_id = ${req.params.id}`;

        const result = await db.query(query);

        return res.status(200).json({
            ok: true,
            data: result
        });
    } catch (err) {
        console.log('Error in getClassById:', err);
        return res.status(500).json({
            ok: false,
            msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
            error: err,
        });
    }
}

const getGeneralsCalificaciones = async (req, res) => {
    try {
        const student_id = req.query.student_id; // Puedes obtener el student_id de tu solicitud
        const mes_id = req.query.mes_id; // Puedes obtener el mes_id de tu solicitud

        // Consulta SQL para obtener el class_id del estudiante
        const getClassIdQuery = 'SELECT class_id FROM student WHERE student_id = ?';
        const classIdResult = await db.query(getClassIdQuery, [student_id]);
        const classId = classIdResult[0].class_id;

        // Consulta SQL para obtener las materias permitidas según el class_id y las notas
        let consultaSQL = '';
        switch (classId) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                consultaSQL = 'SELECT me.mes AS mes, ma.nombre_materia AS materia, ne.nota AS nota FROM notas_escolares ne JOIN mes_escolar me ON ne.mes_id = me.mes_id JOIN materias_escolares ma ON ne.materia_id = ma.materia_id WHERE ne.student_id = ? AND ne.mes_id = ? AND ma.nombre_materia IN ("Lengua Española", "Ingles", "Frances", "Matemática", "Ciencias Sociales", "Ciencias de la Naturaleza", "Formación Integral Humana y Religiosa", "Educación Fisica", "Educación Artística") ORDER BY FIELD(ma.nombre_materia, "Lengua Española", "Ingles", "Frances", "Matemática", "Ciencias Sociales", "Ciencias de la Naturaleza", "Formación Integral Humana y Religiosa", "Educación Fisica", "Educación Artística")';
                break;
            case 11:
            case 12:
                consultaSQL = 'SELECT me.mes AS mes, ma.nombre_materia AS materia, ne.nota AS nota FROM notas_escolares ne JOIN mes_escolar me ON ne.mes_id = me.mes_id JOIN materias_escolares ma ON ne.materia_id = ma.materia_id WHERE ne.student_id = ? AND ne.mes_id = ? AND ma.nombre_materia IN ("Lengua Española", "Ingles", "Frances", "Matemática", "Ciencias Sociales", "Ciencias de la Naturaleza", "Formación Integral Humana y Religiosa", "Educación Fisica", "Educación Artística") ORDER BY FIELD(ma.nombre_materia, "Lengua Española", "Ingles", "Frances", "Matemática", "Ciencias Sociales", "Ciencias de la Naturaleza", "Formación Integral Humana y Religiosa", "Educación Fisica", "Educación Artística")';
                break;
            case 13:
            case 14:
                consultaSQL = 'SELECT me.mes AS mes, ma.nombre_materia AS materia, ne.nota AS nota FROM notas_escolares ne JOIN mes_escolar me ON ne.mes_id = me.mes_id JOIN materias_escolares ma ON ne.materia_id = ma.materia_id WHERE ne.student_id = ? AND ne.mes_id = ? AND ma.nombre_materia IN ("Lengua Española", "Ingles", "Frances", "Matemática", "Ciencias Sociales", "Ciencias de la Naturaleza", "Formación Integral Humana y Religiosa", "Educación Fisica", "Mat. Financiera y tecnologíca", "Educación Artística") ORDER BY FIELD(ma.nombre_materia, "Lengua Española", "Ingles", "Frances", "Matemática", "Ciencias Sociales", "Ciencias de la Naturaleza", "Formación Integral Humana y Religiosa", "Educación Fisica", "Mat. Financiera y tecnologíca", "Educación Artística")';
                break;
            case 15:
                consultaSQL = 'SELECT me.mes AS mes, ma.nombre_materia AS materia, ne.nota AS nota FROM notas_escolares ne JOIN mes_escolar me ON ne.mes_id = me.mes_id JOIN materias_escolares ma ON ne.materia_id = ma.materia_id WHERE ne.student_id = ? AND ne.mes_id = ? AND ma.nombre_materia IN ("Lengua Española", "Ingles", "Frances", "Matemática", "Ciencias Sociales", "Ciencias de la Naturaleza", "Formación Integral Humana y Religiosa", "Educación Fisica", "Educación Artística", "Estadistica, Probabilidad y tecnología") ORDER BY FIELD(ma.nombre_materia, "Lengua Española", "Ingles", "Frances", "Matemática", "Ciencias Sociales", "Ciencias de la Naturaleza", "Formación Integral Humana y Religiosa", "Educación Fisica", "Educación Artística", "Estadistica, Probabilidad y tecnología")';
                break;
            case 16:
                consultaSQL = 'SELECT me.mes AS mes, ma.nombre_materia AS materia, ne.nota AS nota FROM notas_escolares ne JOIN mes_escolar me ON ne.mes_id = me.mes_id JOIN materias_escolares ma ON ne.materia_id = ma.materia_id WHERE ne.student_id = ? AND ne.mes_id = ? AND ma.nombre_materia IN ("Lengua Española", "Ingles", "Frances", "Matemática", "Ciencias Sociales", "Ciencias de la Naturaleza", "Formación Integral Humana y Religiosa", "Educación Fisica", "Educación Artística", "Trigonometria, Cálculo Diferencial y Tecnología", "Estadistica, Probabilidad y tecnología") ORDER BY FIELD(ma.nombre_materia, "Lengua Española", "Ingles", "Frances", "Matemática", "Ciencias Sociales", "Ciencias de la Naturaleza", "Formación Integral Humana y Religiosa", "Educación Fisica", "Educación Artística", "Trigonometria, Cálculo Diferencial y Tecnología", "Estadistica, Probabilidad y tecnología")';
                break;
            default:
                return res.status(400).json({
                    ok: false,
                    msg: 'El class_id del estudiante no es válido.'
                });
        }

        // Realiza la consulta SQL para obtener las notas de las materias permitidas
        const result = await db.query(consultaSQL, [student_id, mes_id]);



        // Formatea los resultados en el formato deseado
        const notasFormateadas = result.map((row) => ({
            mes: row.mes, // Utilizamos el mes de la consulta SQL
            materia: row.materia,
            nota: row.nota, // Utilizamos la nota obtenida de la consulta
        }));
        res.status(200).json(notasFormateadas);

    } catch (err) {
        console.log('Error in getGeneralsCalificaciones:', err);
        return res.status(500).json({
            ok: false,
            msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
            error: err,
        });
    }
}



const getStudentInfo = async(req, res) => {
    try {
        const {id} = req.params;
        const query = `
        SELECT student.first_name AS studentname, student.last_name AS studentlast_name, class.name AS \`class\`
        FROM enroll
        INNER JOIN student ON enroll.student_id = student.student_id
        INNER JOIN \`class\` ON enroll.class_id = \`class\`.class_id
        WHERE enroll.student_id = ?;
      `;
    

      const result = await db.query(query, [id]);

      return res.status(200).json({
        ok: true,
        data: result
      })
    } catch(err) {
        console.log('Error in getStudentInfo:', err);
        return res.status(500).json({
            ok: false,
            msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
            error: err,
        });
    }
}

module.exports = {
    getClass,
    getSemestres,
    getAnio,
    getMesEscolar,
    getMaterias,
    getStudentsByClass,
    getClassById,
    getGeneralsCalificaciones,
    getStudentInfo,
    enviarMensajesATodos
}
const moment = require('moment/moment');
const db = require('../services/db');

const generarReporteVentas = async (req, res) => {
    try {
      const { tipo, fecha } = req.query; // Recibe el tipo de reporte y la fecha desde el query string
      let query = "SELECT * FROM Ventas";
      let queryTotal = 'SELECT SUM(cantidad * precio_venta) as total_ventas FROM Ventas';
      let results = null;
      let totalResults = null;
      if (tipo === 'mensual') {
        // Filtra por mes actual
        query += ` WHERE MONTH(fecha_venta) = ${moment().format('M')} AND YEAR(fecha_venta) = ${moment().format('YYYY')}`;
        queryTotal += ` WHERE MONTH(fecha_venta) = ${moment().format('M')} AND YEAR(fecha_venta) = ${moment().format('YYYY')}`;
        totalResults = await db.query(queryTotal);
        results = await db.query(query);
      } else if (tipo === 'mensual_especifico' && fecha) {
        // Filtra por mes específico (mes y año)
        const mes = moment(fecha).format('M');
        const ano = moment(fecha).format('YYYY');
        query += ` WHERE MONTH(fecha_venta) = ? AND YEAR(fecha_venta) = ?`;
        queryTotal += ` WHERE MONTH(fecha_venta) = ? AND YEAR(fecha_venta) = ?`;
        totalResults = await db.query(queryTotal, [mes, ano]);
        results = await db.query(query, [mes, ano]);
      } else if (tipo === 'diario' && fecha) {
        // Filtra por día específico
        const fechaFormateada = moment(fecha).format('YYYY-MM-DD');
        query += ` WHERE DATE(fecha_venta) = ?`;
        queryTotal += ` WHERE DATE(fecha_venta) = ?`;
        totalResults = await db.query(queryTotal, [fechaFormateada]);
        results = await db.query(query, [fechaFormateada]);
      } else if (tipo === 'anual' && fecha) {
        // Filtra por año específico
        const ano = moment(fecha).format('YYYY');
        query += ` WHERE YEAR(fecha_venta) = ?`;
        queryTotal += ` WHERE YEAR(fecha_venta) = ?`;
        totalResults = await db.query(queryTotal, [ano]);
        results = await db.query(query, [ano]);
      }
      
      return res.status(200).json({
        ok: true,
        data: results,
        total: totalResults
      });
    } catch (err) {
      console.error('Error en generarReporteVentas:', err);
      return res.status(500).json({
        ok: false,
        msg: 'Se ha producido un error inesperado, por favor contacta al administrador del sistema.',
        error: err,
      });
    }
  };

  const generarReporteReceipts = async (req, res) => {
    try {
      const { tipo, fecha } = req.query; // Recibe el tipo de reporte y la fecha desde el query string

      let query = 'SELECT * FROM receipts';
      let sumQuery = 'SELECT SUM(monto) AS monto_total FROM receipts';
      let month = moment(fecha).format('MM');
      let year = moment(fecha).format('YYYY')
      
      let fechaFormateada = ''; // Variable para almacenar la fecha formateada con Moment.js
      
      if (fecha) {
        fechaFormateada = moment(fecha, 'YYYY-MM').format('YYYY-MM');
      }
      
      if (tipo === 'mensual') {
        // Filtra por mes actual
        query += ` WHERE MONTH(fecha_generacion) = MONTH(CURRENT_DATE()) AND YEAR(fecha_generacion) = YEAR(CURRENT_DATE())`;
        sumQuery += ` WHERE MONTH(fecha_generacion) = MONTH(CURRENT_DATE()) AND YEAR(fecha_generacion) = YEAR(CURRENT_DATE())`;
      } else if (tipo === 'mensual_especifico' && fecha) {
        // Filtra por mes específico (mes y año)
        query += ` WHERE MONTH(fecha_generacion) = ? AND YEAR(fecha_generacion) = ?`;
        sumQuery += ` WHERE MONTH(fecha_generacion) = ? AND YEAR(fecha_generacion) = ?`;
      } else if (tipo === 'diario' && fecha) {
        // Filtra por día específico
        query += ` WHERE DATE(fecha_generacion) = ?`;
        sumQuery += ` WHERE DATE(fecha_generacion) = ?`;
      } else if (tipo === 'anual' && fecha) {
        // Filtra por año específico
        query += ` WHERE YEAR(fecha_generacion) = ?`;
        sumQuery += ` WHERE YEAR(fecha_generacion) = ?`;
      }
      
      // Ejecuta la consulta y obtén los resultados
      console.log([fechaFormateada]); // Fecha formateada con Moment.js
      const results = await db.query(query, fecha ? [month, year] : []);
      
      // Calcula el monto total de ventas
      const sumaResult = await db.query(sumQuery, fecha ? [month, year] : []);
      const montoTotal = sumaResult[0].monto_total || 0;
      
      return res.status(200).json({
        ok: true,
        data: results,
        montoTotal,
      });      
    } catch (err) {
      console.error('Error en generarReporteReceipts:', err);
      return res.status(500).json({
        ok: false,
        msg: 'Se ha producido un error inesperado, por favor contacta al administrador del sistema.',
        error: err,
      });
    }
  };

  const cxc = async(req, res) => {
    try {
      const fechaActual = new Date();
      const nombreMes = fechaActual.toLocaleString('es-ES', { month: 'long' }).toLowerCase();

      const consultaSQL = `
  SELECT *
  FROM cuotas_mensuales
  WHERE LOWER(mes) = ? AND estado = 'PE'
`;

const result = await db.query(consultaSQL, [req.query.mes.toLowerCase()]);

res.status(200).json({
  ok: true,
  data: result
});
      
    } catch(err) {
      console.error('Error en cxc:', err);
      return res.status(500).json({
        ok: false,
        msg: 'Se ha producido un error inesperado, por favor contacta al administrador del sistema.',
        error: err,
      });
    }
  }

  const getTotalMatriculas = async(req, res) => {
    try {
      const query = `SELECT student_id FROM student WHERE status = 'a'`;

      const result = await db.query(query);

      return res.status(200).json({
        ok: true,
        data: result.length
      });
    } catch(err) {
      console.error('Error en getTotalMatriculas:', err);
      return res.status(500).json({
        ok: false,
        msg: 'Se ha producido un error inesperado, por favor contacta al administrador del sistema.',
        error: err,
      });
    }
  }

  module.exports = {
    generarReporteVentas,
    generarReporteReceipts,
    cxc,
    getTotalMatriculas
  }
const helper = require('../../helper');
const db = require('../services/db');

const registrarAdmision = async (req, res) => {
    try {
      const { first_name, last_name, birthday, sex, address, phone, email, parent_id, mother_id, class_id, conditions,
        allergies,
        doctor_name,
        doctor_phone,
        authorized_person,
        authorized_person_phone,
        notes } = req.body;
  
      // Verificar si ya existe una admisión con el mismo correo electrónico
      // const admisionExistente = await db.query(
      //   `SELECT * FROM student WHERE email = '${email}'`
      // );
  
      // if (admisionExistente.length > 0) {
      //   return res.status(400).json({
      //     ok: false,
      //     msg: 'Ya existe una admisión registrada con el mismo correo electrónico',
      //   });
      // }
  
      // Generar el username y la contraseña
      const username = generateUsername(first_name.toLowerCase(), last_name.toLowerCase());
  
      // Insertar la admisión en la tabla "admision"
      const insertQuery = `
        INSERT INTO student (
          first_name,
          last_name,
          birthday,
          sex,
          address,
          phone,
          email,
          password,
          username,
          parent_id,
          status,
          inscripcion_estado,
          mother_id,
          diseases,
          allergies,
          doctor,
          doctor_phone,
          authorized_person,
          authorized_phone,
          note,
          class_id
        ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
      `;
  
      const values = [
        first_name,
        last_name,
        birthday,
        sex,
        address,
        phone,
        email,
        helper.generatePassword(),
        username,
        parent_id,
        'a',
        0,
        mother_id,
        conditions,
        allergies,
        doctor_name,
        doctor_phone,
        authorized_person,
        authorized_person_phone,
        notes,
        class_id
      ];

      console.log(values);
      const student = await db.query(insertQuery, values);
      console.log(student);
  
      return res.status(200).json({
        ok: true,
        msg: 'Admisión registrada exitosamente',
        user: "El usuario creado para este usuario es" + " " + username + " " + "y la contraseña es CB123"
      });
    } catch (err) {
      console.log('Error in registrarAdmision:', err);
      return res.status(500).json({
        ok: false,
        msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
        error: err,
      });
    }
  };
  
  // Función para generar el username
  const generateUsername = (first_name, last_name) => {
    const usernamePrefix = `${first_name.charAt(0)}${last_name.replace(/ /g, '')}`;
    return `${usernamePrefix}`;
  };

  function generarNumeroOnceDigitos() {
    const min = 10000000000; // El número más pequeño de 11 dígitos (10^10)
    const max = 99999999999; // El número más grande de 11 dígitos (10^11 - 1)
  
    // Genera un número aleatorio entre min y max
    const numeroAleatorio = Math.floor(Math.random() * (max - min + 1)) + min;
  
    return numeroAleatorio.toString(); // Convierte el número a una cadena de texto
  }

  const registrarPadre = async (req, res) => {
    try {
      const {
        first_name,
        last_name,
        email,
        phone,
        address,
        profession,
        gender,
        business,
        idcard,
        business_phone,
        home_phone
      } = req.body;

      if(idcard == '' || idcard == null || idcard == undefined) idcard = generarNumeroOnceDigitos();
  
      const username = generateUsername(first_name.toLowerCase(), last_name.toLowerCase())

      // Verificar si ya existe un padre con el mismo correo electrónico
      // const padreExistente = await db.query(
      //   `SELECT * FROM parent WHERE email = '${email}'`
      // );
  
      // if (padreExistente.length > 0) {
      //   return res.status(400).json({
      //     ok: false,
      //     msg: 'Ya existe un padre registrado con el mismo correo electrónico',
      //   });
      // }
  
      // Verificar si ya existe un padre con el mismo número de identificación (idcard)
      // const padreExistenteIdCard = await db.query(
      //   `SELECT * FROM parent WHERE idcard = '${idcard}'`
      // );
  
      // if (padreExistenteIdCard.length > 0) {
      //   return res.status(400).json({
      //     ok: false,
      //     msg: 'Ya existe un padre registrado con el mismo número de identificación',
      //   });
      // }
  
      // Insertar el padre en la tabla "parent"

      // href="https://calificaciones.colebuenosaires.com/app/cursos" target="_blank"

      const insertQuery = `
        INSERT INTO parent (
          first_name,
          last_name,
          email,
          password,
          phone,
          address,
          profession,
          username,
          business,
          idcard,
          business_phone,
          home_phone
        ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
      `;
  
      const values = [
        first_name,
        last_name,
        email,
        helper.generatePassword(),
        phone,
        address,
        profession,
        username,
        business,
        idcard,
        business_phone,
        home_phone
      ];
  
      await db.query(insertQuery, values);
  
      return res.status(200).json({
        ok: true,
        msg: 'Padre registrado exitosamente',
        user: "El usuario creado para este usuario es" + " " + username + " " + "y la contraseña es CB123"
      });
    } catch (err) {
      console.log('Error in registrarPadre:', err);
      return res.status(500).json({
        ok: false,
        msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
        error: err,
      });
    }
  };
  
  const consultarPadres = async (req, res) => {
    try {
      // Consultar todos los padres en la tabla "parent"
      const query = `SELECT * FROM parent`;
      const padres = await db.query(query);
  
      return res.status(200).json({
        ok: true,
        padres,
      });
    } catch (err) {
      console.log('Error in consultarPadres:', err);
      return res.status(500).json({
        ok: false,
        msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
        error: err,
      });
    }
  };

  const consutarGrados = async(req, res) => {
    try {
      const query = `SELECT * FROM class`;
      const classTable = await db.query(query);
  
      return res.status(200).json({
        ok: true,
        class: classTable,
      });
    } catch(err) {
      console.log('Error in consutarGrados:', err);
      return res.status(500).json({
        ok: false,
        msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
        error: err,
      });
    }
  }

  function generarCodigoAlfanumerico(y) {
    const caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let codigo = '';
  
    for (let i = 0; i < y; i++) {
      const randomIndex = Math.floor(Math.random() * caracteres.length);
      codigo += caracteres.charAt(randomIndex);
    }
  
    return codigo;
  }
  

  module.exports = {
    registrarAdmision,
    registrarPadre,
    consultarPadres,
    consutarGrados
  }
  
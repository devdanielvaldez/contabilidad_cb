const db = require('../services/db');

const getAllEmployes = async(req, res) => {
    try {
        const queryTeachers = `SELECT teacher_id, first_name, last_name, phone, email, salario, tss, seguro, afp, no_socio, no_account, idcard FROM teacher`;
        const queryAccount = `SELECT accountant_id, first_name, last_name, phone, email, salario, tss, seguro, afp, no_socio, no_account, idcard FROM accountant`;

        const dataTeachers = await db.query(queryTeachers);
        const dataAccounts = await db.query(queryAccount);

        const teacherArrayWithType = dataTeachers.map(teacher => ({
            ...teacher,
            type: 't'
          }));

          const accountantArrayWithType = dataAccounts.map(account => ({
            ...account,
            type: 'a'
          }));

        const mergeArray = teacherArrayWithType.concat(accountantArrayWithType);

        return res.status(200).json({
            ok: true,
            data: mergeArray
        });
    } catch(err) {
        console.log('Error in getAllEmployes:', err);
        return res.status(500).json({
          ok: false,
          msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
          error: err,
        });
    }
}

const saveSalary = async(req, res) => {
    try {
        const {
            type,
            salary,
            id
        } = req.body;

        if(type == 't') {
            await db.query(`UPDATE teacher SET salario = ${salary} WHERE teacher_id = ${id}`);
            return res.status(200).json({
                ok: true,
                msg: "Salario actualizado correctamente"
            });
        } else {
            await db.query(`UPDATE accountant SET salario = ${salary} WHERE accountant_id = ${id}`);
            return res.status(200).json({
                ok: true,
                msg: "Salario actualizado correctamente"
            });
        }
    } catch(err) {
        console.log('Error in saveSalary:', err);
        return res.status(500).json({
          ok: false,
          msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
          error: err,
        });
    }
}

const saveTSS = async(req, res) => {
    try {
        const {
            type,
            tss,
            id
        } = req.body;

        if(type == 't') {
            await db.query(`UPDATE teacher SET tss = ${tss} WHERE teacher_id = ${id}`);
            return res.status(200).json({
                ok: true,
                msg: "TSS actualizado correctamente"
            });
        } else {
            await db.query(`UPDATE accountant SET tss = ${tss} WHERE accountant_id = ${id}`);
            return res.status(200).json({
                ok: true,
                msg: "TSS actualizado correctamente"
            });
        }
    } catch(err) {
        console.log('Error in saveTSS:', err);
        return res.status(500).json({
          ok: false,
          msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
          error: err,
        });
    }
}

const saveSeguro = async(req, res) => {
    try {
        const {
            type,
            seguro,
            id
        } = req.body;

        if(type == 't') {
            await db.query(`UPDATE teacher SET seguro = ${seguro} WHERE teacher_id = ${id}`);
            return res.status(200).json({
                ok: true,
                msg: "Seguro actualizado correctamente"
            });
        } else {
            await db.query(`UPDATE accountant SET seguro = ${seguro} WHERE accountant_id = ${id}`);
            return res.status(200).json({
                ok: true,
                msg: "Seguro actualizado correctamente"
            });
        }
    } catch(err) {
        console.log('Error in saveSeguro:', err);
        return res.status(500).json({
          ok: false,
          msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
          error: err,
        });
    }
}

const saveAFP = async(req, res) => {
    try {
        const {
            type,
            afp,
            id
        } = req.body;

        if(type == 't') {
            await db.query(`UPDATE teacher SET afp = ${afp} WHERE teacher_id = ${id}`);
            return res.status(200).json({
                ok: true,
                msg: "AFP actualizado correctamente"
            });
        } else {
            await db.query(`UPDATE accountant SET afp = ${afp} WHERE accountant_id = ${id}`);
            return res.status(200).json({
                ok: true,
                msg: "AFP actualizado correctamente"
            });
        }
    } catch(err) {
        console.log('Error in saveAFP:', err);
        return res.status(500).json({
          ok: false,
          msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
          error: err,
        });
    }
}

const saveSocioId = async(req, res) => {
    try {
        const {
            type,
            socio_id,
            id
        } = req.body;

        if(type == 't') {
            await db.query(`UPDATE teacher SET no_socio = ${socio_id} WHERE teacher_id = ${id}`);
            return res.status(200).json({
                ok: true,
                msg: "No. Socio actualizado correctamente"
            });
        } else {
            await db.query(`UPDATE accountant SET no_socio = ${socio_id} WHERE accountant_id = ${id}`);
            return res.status(200).json({
                ok: true,
                msg: "No. Socio actualizado correctamente"
            });
        }
    } catch(err) {
        console.log('Error in saveSocioId:', err);
        return res.status(500).json({
          ok: false,
          msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
          error: err,
        });
    }
}

const saveAccount = async(req, res) => {
    try {
        const {
            type,
            account,
            id
        } = req.body;

        if(type == 't') {
            await db.query(`UPDATE teacher SET no_account = ${account} WHERE teacher_id = ${id}`);
            return res.status(200).json({
                ok: true,
                msg: "No. de Cuenta actualizado correctamente"
            });
        } else {
            await db.query(`UPDATE accountant SET no_account = ${account} WHERE accountant_id = ${id}`);
            return res.status(200).json({
                ok: true,
                msg: "No. de Cuenta actualizado correctamente"
            });
        }
    } catch(err) {
        console.log('Error in saveAccount:', err);
        return res.status(500).json({
          ok: false,
          msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
          error: err,
        });
    }
}

module.exports = {
    getAllEmployes,
    saveSalary,
    saveTSS,
    saveSeguro,
    saveAFP,
    saveSocioId,
    saveAccount
}
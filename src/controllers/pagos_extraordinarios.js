const helper = require('../../helper');
const db = require('../services/db');

const registrarEvento = async (req, res) => {
  try {
    const { descripcion, codigo, costo, fecha_limite } = req.body;

    const insertEventoQuery = `
        INSERT INTO eventos_pagos (descripcion, codigo, costo, fecha_limite)
        VALUES (?, ?, ?, ?)
      `;
    const eventoValues = [descripcion, codigo, costo, fecha_limite];

    // Registrar en eventos_pagos
    await db.query(insertEventoQuery, eventoValues);

    return res.status(200).json({
      ok: true,
      msg: 'Evento registrado exitosamente',
    });
  } catch (err) {
    console.log('Error in registrarEvento:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const buscarEventos = async (req, res) => {
  try {
    const selectEventosQuery = `SELECT * FROM eventos_pagos;`;
    const eventos = await db.query(selectEventosQuery);

    const selectAsignacionesQuery = `SELECT event_id FROM asignacion_eventos;`;
    const asignaciones = await db.query(selectAsignacionesQuery);

    const eventosConAsignacion = eventos.map(evento => ({
      ...evento,
      asignado: asignaciones.some(asignacion => asignacion.event_id === evento.eventos_p_id)
    }));

    return res.status(200).json({
      ok: true,
      eventos: eventosConAsignacion,
    });
  } catch (err) {
    console.log('Error in buscarEventos:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const asignarEventos = async (req, res) => {
  try {
    const { class_ids, event_id } = req.body;

    if (!class_ids || !event_id) {
      return res.status(400).json({ message: 'Datos incompletos en la solicitud' });
    }

    const insertQuery = `
      INSERT INTO asignacion_eventos (class_id, event_id)
      VALUES (?, ?)
    `;

    // Realizar la inserción en la tabla asignacion_eventos para cada class_id
    await class_ids.map(async (class_id) => {
        await db.query(insertQuery, [class_id, event_id], (err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve(result);
          }
        });
    });

    return res.status(200).json({
      ok: true,
      msg: 'Asignaciones registradas exitosamente',
    });
  } catch (err) {
    console.log('Error in asignarEventos:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const obtenerEstudiantesPorEvento = async (req, res) => {
  try {
    const { evento_p_id } = req.params;

    const getEventInfo = `SELECT * FROM eventos_pagos WHERE eventos_p_id = ${evento_p_id}`;
    const getEvent = await db.query(getEventInfo);

    const obtenerClassInfoQuery = `
      SELECT ae.class_id, c.name
      FROM asignacion_eventos ae
      INNER JOIN class c ON ae.class_id = c.class_id
      WHERE ae.event_id = ?
    `;

    const obtenerStudentIdsQuery = `
      SELECT student_id
      FROM enroll
      WHERE class_id = ?
    `;

    const obtenerStudentInfoQuery = `
      SELECT student_id, first_name, last_name, parent_id, mother_id
      FROM student
      WHERE student_id = ?
    `;

    const obtenerPagosEventosQuery = `
      SELECT student_id
      FROM pagos_eventos
      WHERE event_id = ?
    `;

    // Obtener información de la clase (class_id y nombre) asociada al evento
    const classInfoResult = await db.query(obtenerClassInfoQuery, [evento_p_id]);
    const classInfo = classInfoResult.map(row => ({
      class_id: row.class_id,
      name: row.name
    }));

    // Obtener datos de los estudiantes asociados a cada clase
    const studentDataByClass = [];
    for (const classInfoRow of classInfo) {
      const studentIdsResult = await db.query(obtenerStudentIdsQuery, [classInfoRow.class_id]);
      const studentIds = studentIdsResult.map(row => row.student_id);

      const studentData = [];
      for (const studentId of studentIds) {
        // Verificar si el student_id está en la tabla pagos_eventos
        const pagosEventosResult = await db.query(obtenerPagosEventosQuery, [evento_p_id]);
        const pagosEventosStudentIds = pagosEventosResult.map(row => row.student_id);
        
        if (!pagosEventosStudentIds.includes(studentId)) {
          const studentInfoResult = await db.query(obtenerStudentInfoQuery, [studentId]);
          const studentInfo = studentInfoResult[0];
          studentData.push(studentInfo);
        }
      }

      if (studentData.length > 0) {
        studentDataByClass.push({
          title: classInfoRow.name,
          students: studentData
        });
      }
    }

    // Eliminar elementos null de studentDataByClass
    const filteredStudentDataByClass = studentDataByClass.filter(item => item.students !== null);

    return res.status(200).json({
      ok: true,
      data: filteredStudentDataByClass,
      event_id: evento_p_id,
      event: getEvent
    });
  } catch (err) {
    console.log('Error in obtenerEstudiantesPorEvento:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};


const registrarPagoEvento = async (req, res) => {
  try {
    const { event_id, student_id, monto, descripcion, metodo_de_pago, no_recibo, type } = req.body;

    // Registrar el pago en la tabla "pagos_eventos"
    const insertPagosEventosQuery = `
      INSERT INTO pago_eventos (evento_id, student_id, fecha_pago)
      VALUES (?, ?, ?)
    `;
    const currentTimestamp = new Date().toISOString().slice(0, 19).replace('T', ' '); // Formato MySQL timestamp
    const pagosEventosValues = [event_id, student_id, currentTimestamp];
    await db.query(insertPagosEventosQuery, pagosEventosValues);

    // Registrar el recibo de pago en la tabla "receipts"
    const insertReceiptQuery = `
      INSERT INTO receipts (numero_recibo, fecha_generacion, monto, descripcion, student_id, metodo_de_pago, type)
      VALUES (?, ?, ?, ?, ?, ?, ?)
    `;
    const receiptValues = [no_recibo, currentTimestamp, monto, descripcion, student_id, metodo_de_pago, type];
    await db.query(insertReceiptQuery, receiptValues);

    return res.status(200).json({
      ok: true,
      msg: 'Pago registrado exitosamente',
    });
  } catch (err) {
    console.log('Error in registrarPagoEvento:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const obtenerPagosPorEvento = async (req, res) => {
  try {
    const { evento_id } = req.params;

    const obtenerPagosQuery = `
      SELECT pe.pago_id, pe.fecha_pago, pe.student_id,
             s.first_name, s.last_name, s.parent_id, s.mother_id
      FROM pago_eventos pe
      INNER JOIN student s ON pe.student_id = s.student_id
      WHERE pe.evento_id = ?
    `;

    const pagosResult = await db.query(obtenerPagosQuery, [evento_id]);

    return res.status(200).json({
      ok: true,
      data: pagosResult,
    });
  } catch (err) {
    console.log('Error in obtenerPagosPorEvento:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

module.exports = {
  registrarEvento,
  buscarEventos,
  asignarEventos,
  obtenerEstudiantesPorEvento,
  registrarPagoEvento,
  obtenerPagosPorEvento
}
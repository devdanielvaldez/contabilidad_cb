const helper = require('../../helper');
const db = require('../services/db');

const getStudentPendingPayment = async (req, res) => {
  try {
    const currentDate = new Date();
    const months = [
      "enero", "febrero", "marzo", "abril", "mayo", "junio",
      "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"
    ];
    const currentMonthLast = currentDate.getMonth() - 1; 
    const currentMonth = currentDate.getMonth(); // Ten en cuenta que los meses en JavaScript son 0-indexados
    const monthInText = months[currentMonth];
    const monthInTextLast = months[currentMonthLast];
    
    // `monthInText` ahora contendrá el nombre del mes actual en texto, por ejemplo, "octubre"
    
    console.log(monthInText);

    const pendingInvoices = await db.query(`
    SELECT 
    cm.*, 
    s.first_name AS student_first_name, 
    s.last_name AS student_last_name,
    p.first_name AS parent_first_name, 
    p.last_name AS parent_last_name
FROM 
    cuotas_mensuales cm
JOIN 
    student s ON cm.student_id = s.student_id
JOIN 
    parent p ON s.parent_id = p.parent_id
WHERE 
    cm.estado = 'PE' AND cm.mes = '${monthInText}';
    `);

    pendingInvoices.forEach(item => {
      if (item.mes === null || item.mes === "") {
        item.mes = "diciembre";
      }
    });

    return res.status(200).json({
      ok: true,
      data: pendingInvoices,
    });
  } catch (err) {
    console.error('Error in getStudentPendingPayment:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const getStudentPendintPayment = async(req, res) => {
  try {
    const { student_id } = req.params;

    const pendintInvoce = await db.query(`SELECT * FROM cuotas_mensuales WHERE student_id = '${student_id}' AND estado = 'PE'`);
    pendintInvoce.forEach(item => {
      if (item.mes === null || item.mes === "") {
        item.mes = "diciembre";
      }
    });
    return res.status(200).json({
      ok: true,
      data: pendintInvoce
    });
  } catch(err) {
    console.log('Error in getStudentPendintPayment:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
}

const registrarInventario = async (req, res) => {
  try {
    const { codigo, descripcion, fecha, cantidad, precio } = req.body;

    const existProduct = await db.query(`SELECT * FROM Inventario WHERE codigo = '${codigo}' AND estado = 'IN'`);

    if(existProduct.length > 0) return res.status(400).json({
      ok: false,
      msg: 'El producto que desea ingresar ya se encuentra registrado pero INACTIVO.'
    })

    // Insertar el artículo en la tabla "Inventario"
    const insertQuery = `
      INSERT INTO Inventario (codigo, descripcion, fecha, cantidad, precio)
      VALUES (?, ?, ?, ?, ?)
    `;

    const values = [codigo, descripcion, fecha, cantidad, precio];

    await db.query(insertQuery, values);

    return res.status(200).json({
      ok: true,
      msg: 'Artículo registrado en el inventario exitosamente',
    });
  } catch (err) {
    console.log('Error in registrarInventario:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const registrarVenta = async (req, res) => {
  try {
    const { nombre_cliente, articulos, tanda } = req.body;

    if (!nombre_cliente || !Array.isArray(articulos) || articulos.length === 0) {
      return res.status(400).json({
        ok: false,
        msg: 'El cuerpo de la solicitud debe contener "nombre_cliente" y una lista de "articulos".',
      });
    }

    let totalVenta = 0;
    const ventasRegistradas = [];

    for (const { codigo_articulo, cantidad } of articulos) {
      // Obtener el artículo del inventario
      const articuloQuery = `
        SELECT codigo, descripcion, precio, cantidad FROM Inventario WHERE codigo = '${codigo_articulo}'
      `;

      const articuloResult = await db.query(articuloQuery);

      if (articuloResult.length === 0) {
        return res.status(404).json({
          ok: false,
          msg: `No se encontró el artículo '${codigo_articulo}' en el inventario`,
        });
      }

      const { codigo, descripcion, precio, cantidad: stock } = articuloResult[0];

      // Verificar si hay suficiente stock del artículo para realizar la venta
      if (stock < cantidad) {
        return res.status(400).json({
          ok: false,
          msg: `No hay suficiente stock del artículo '${codigo_articulo}' para realizar la venta`,
        });
      }

      const precio_venta = precio;
      const total_articulo = precio_venta * cantidad;
      totalVenta += total_articulo;

      // Insertar la venta en la tabla "Ventas"
      const insertQuery = `
        INSERT INTO Ventas (codigo_articulo, cantidad, nombre_cliente, precio_venta, fecha_venta, tanda)
        VALUES (?, ?, ?, ?, CURDATE(), ?)
      `;

      const values = [codigo_articulo, cantidad, nombre_cliente, precio_venta, tanda];

      await db.query(insertQuery, values);

      // Actualizar la cantidad en el inventario
      const updateQuery = `
        UPDATE Inventario
        SET cantidad = cantidad - ?
        WHERE codigo = ?
      `;

      const updateValues = [cantidad, codigo_articulo];

      await db.query(updateQuery, updateValues);

      ventasRegistradas.push({
        codigo,
        descripcion,
        cantidad,
        precio: precio_venta,
        total: total_articulo,
      });
    }

    return res.status(200).json({
      ok: true,
      msg: 'Ventas registradas exitosamente',
      cliente: nombre_cliente,
      ventas: ventasRegistradas,
      totalVenta,
    });
  } catch (err) {
    console.log('Error in registrarVenta:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const obtenerGananciasPorMes = async (req, res) => {
  try {
    // Obtener el total de ganancias por mes
    const query = `
        SELECT MONTH(fecha_venta) AS mes, YEAR(fecha_venta) AS anio, SUM(precio_venta * cantidad) AS total_ganancias
        FROM Ventas
        GROUP BY MONTH(fecha_venta), YEAR(fecha_venta)
      `;

    const query_ventas = 'SELECT * FROM Ventas';

    const resultados = await db.query(query);
    const ventas = await db.query(query_ventas);

    // Obtener las ganancias totales hasta la fecha actual
    const gananciasTotalesQuery = `
        SELECT SUM(precio_venta * cantidad) AS total_ganancias
        FROM Ventas
        WHERE fecha_venta <= CURDATE()
      `;

    const gananciasTotalesResult = await db.query(gananciasTotalesQuery);
    const gananciasTotalesVentas = gananciasTotalesResult[0].total_ganancias || 0;

    // Obtener las ganancias totales de colegiatura
    const gananciasTotalesColegiaturaQuery = `
        SELECT SUM(monto) AS total_ganancias_colegiatura
        FROM historial_de_pago
        WHERE tipo_pago = 'colegiatura'
      `;

    const gananciasTotalesColegiaturaResult = await db.query(gananciasTotalesColegiaturaQuery);
    const gananciasTotalesColegiatura = gananciasTotalesColegiaturaResult[0].total_ganancias_colegiatura || 0;

    // Obtener el artículo más vendido
    const articuloMasVendidoQuery = `
        SELECT codigo_articulo, SUM(cantidad) AS total_vendido
        FROM Ventas
        GROUP BY codigo_articulo
        ORDER BY total_vendido DESC
        LIMIT 1
      `;

    const articuloMasVendidoResult = await db.query(articuloMasVendidoQuery);

    let articuloMasVendido = null;
    if (articuloMasVendidoResult.length > 0) {
      const codigoArticuloMasVendido = articuloMasVendidoResult[0].codigo_articulo;
      const articuloQuery = `SELECT * FROM Inventario WHERE codigo = "${codigoArticuloMasVendido}"`;
      const articuloResult = await db.query(articuloQuery);
      articuloMasVendido = articuloResult[0];
    }

    return res.status(200).json({
      ok: true,
      gananciasPorMes: resultados,
      gananciasTotalesVentas,
      gananciasTotalesColegiatura,
      articuloMasVendido,
      ventas: ventas
    });
  } catch (err) {
    console.log('Error in obtenerGananciasPorMes:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const verInventario = async (req, res) => {
  try {
    // Obtener todos los registros del inventario
    const query = `
        SELECT codigo, descripcion, fecha, cantidad, precio
        FROM Inventario WHERE estado = 'AC'
      `;

    const resultados = await db.query(query);

    return res.status(200).json({
      ok: true,
      inventario: resultados,
    });
  } catch (err) {
    console.log('Error in verInventario:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const cambiarEstadoPorCodigo = async (req, res) => {
  try {
    const { codigo } = req.params;

    // Verificar si el artículo con el código existe en el inventario
    const articuloExistente = await db.query(
      `SELECT * FROM Inventario WHERE codigo = '${codigo}'`
    );

    if (articuloExistente.length === 0) {
      return res.status(404).json({
        ok: false,
        msg: 'Artículo no encontrado en el inventario',
      });
    }

    // Realizar la actualización del estado a "IN"
    await db.query(
      `UPDATE Inventario
       SET estado = 'IN'
       WHERE codigo = '${codigo}'`
    );

    return res.status(200).json({
      ok: true,
      msg: 'Estado del artículo actualizado correctamente',
    });
  } catch (err) {
    console.log('Error in cambiarEstadoPorCodigo:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const editarArticuloInventario = async (req, res) => {
  try {
    const { codigo } = req.query;
    const { descripcion, fecha, cantidad, precio } = req.body;

    // Verificar si el artículo con el código existe en el inventario
    const articuloExistente = await db.query(
      `SELECT * FROM Inventario WHERE codigo = '${codigo}'`
    );

    if (articuloExistente.length === 0) {
      return res.status(404).json({
        ok: false,
        msg: 'Artículo no encontrado en el inventario',
      });
    }

    // Realizar la actualización del artículo en el inventario
    await db.query(
      `UPDATE Inventario
       SET descripcion = '${descripcion}',
           fecha = '${fecha}',
           cantidad = ${cantidad},
           precio = ${precio}
       WHERE codigo = '${codigo}'`
    );

    return res.status(200).json({
      ok: true,
      msg: 'Datos del artículo actualizados correctamente',
    });
  } catch (err) {
    console.log('Error in editarArticuloInventario:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const obtenerGananciasPorDia = async (req, res) => {
  try {
    const { fecha, tanda } = req.body; // La fecha debe estar en formato 'YYYY-MM-DD'

    // Verificar si la fecha es válida
    if (!fecha || !/^\d{4}-\d{2}-\d{2}$/.test(fecha)) {
      return res.status(400).json({
        ok: false,
        msg: 'Fecha inválida. La fecha debe estar en formato "YYYY-MM-DD".',
      });
    }

    // Consulta para obtener el total de ganancias, el artículo más vendido y la cantidad total de artículos vendidos para la fecha especificada
    const query = `
      SELECT 
          fecha_venta AS fecha,
          codigo_articulo,
          SUM(precio_venta * cantidad) AS total_ganancias,
          SUM(cantidad) AS cantidad_total_vendida
      FROM 
          Ventas
      WHERE
          DATE(fecha_venta) = '${fecha}' AND tanda = '${tanda}'
      GROUP BY
          fecha_venta,
          codigo_articulo;
    `;

    const total_ganancias = `SELECT SUM(precio_venta * cantidad) AS total_ganancias
    FROM Ventas
    WHERE DATE(fecha_venta) = '${fecha}' AND tanda = '${tanda}';
    `

    // Ejecutar la consulta en la base de datos (asumiendo que estás utilizando algún cliente de base de datos como 'mysql2' o 'mysql')
    const resultados = await db.query(query);
    const ganancias = await db.query(total_ganancias);

    if (resultados.length === 0) {
      return res.status(404).json({
        ok: false,
        msg: 'No se encontraron registros para la fecha especificada.',
      });
    }

    return res.status(200).json({
      ok: true,
      ventasPorFecha: resultados,
      total: ganancias[0].total_ganancias
    });
  } catch (err) {
    console.log('Error in obtenerGananciasPorDia:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
};

const register_receipts = async(req, res) => {
  try {
    const {
      numero_recibo,
      monto,
      descripcion,
      student_id,
      metodo,
      type,
      time_type
    } = req.body;

    console.log(req.body);

    await db.query(`INSERT INTO receipts (numero_recibo, monto, descripcion, student_id, metodo_de_pago, type, time_type) VALUES (?, ?, ?, ?, ?, ?, ?)`, [
      numero_recibo,
      monto,
      descripcion,
      +student_id,
      metodo,
      type,
      time_type
    ]);

    return res.status(200).json({
      ok: true,
      msg: "Recibo registrado correctamente"
    });
  } catch(err) {
    console.log('Error in register_receipts:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
}

const getReceipts = async(req, res) => {
  try {
    const {
      student_id
    } = req.params;

    const receipts = await db.query(`SELECT * FROM receipts WHERE student_id = ${student_id}`);
    const student = await db.query(`SELECT * FROM student WHERE student_id = ${student_id}`);
    const receiptsArrayWithType = receipts.map(r => ({
      ...r,
      student: student[0]
    }));
    return res.status(200).json({
      ok: true,
      data: receiptsArrayWithType
    });
  } catch(err) {
    console.log('Error in getReceipts:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
}

const getReportsReceipts = async (req, res) => {
  try {
    const { fecha, time_type } = req.query;
    
    // Usar consulta preparada para evitar inyección SQL
    const selectReceipts = `SELECT * FROM receipts WHERE DATE(fecha_generacion) = ? AND time_type = ?;`;
    const receipts_responde = await db.query(selectReceipts, [fecha, time_type]);
    
    // También se debe usar una consulta preparada aquí
    const receipts_amount_query = `SELECT SUM(monto) AS ganancias_totales FROM receipts WHERE DATE(fecha_generacion) = ? AND time_type = ?;`;
    const receipts_amount = await db.query(receipts_amount_query, [fecha, time_type]);
    
    return res.status(200).json({
      ok: true,
      data: receipts_responde,
      amount: receipts_amount[0].ganancias_totales
    });
  } catch (err) {
    console.log('Error in getReportsReceipts:', err);
    return res.status(500).json({
      ok: false,
      msg: 'Se ha producido un error inesperado, por favor contacte al administrador del sistema.',
      error: err,
    });
  }
}




module.exports = {
  registrarInventario,
  registrarVenta,
  obtenerGananciasPorMes,
  verInventario,
  cambiarEstadoPorCodigo,
  editarArticuloInventario,
  obtenerGananciasPorDia,
  register_receipts,
  getReceipts,
  getReportsReceipts,
  getStudentPendintPayment,
  getStudentPendingPayment
}